#!/usr/bin/env python3
"""Tests for Chanterelle."""

import collections
import contextlib
import io
import mimetypes
import os
import unittest
import unittest.mock

import chanterelle


# mock Boto bucket object -- only used fields are included
BucketObject = collections.namedtuple('BucketObject', ('key', 'e_tag'))


class SyncBucketTest(unittest.TestCase):
    """Tests for sync_bucket()."""
    SETTINGS = {
        'site_root': '_site',
        'charset': 'utf-8',
        'strip_html': False,
        'delete_old': False
    }

    def setUp(self):
        self.bucket_mock = unittest.mock.Mock()
        self.bucket_mock.name = 'test.bucket'
        self.bucket_mock.objects.all.return_value = [
            BucketObject('index.html', '"d41d8cd98f00b204e9800998ecf8427e"')
        ]  # empty file

        self.mime_types = mimetypes.MimeTypes()
        self.mime_types.types_map = ({}, {'.html': 'text/html'})  # replace ALL

    def test_empty_upload(self):
        """Files are uploaded when not present in the bucket."""
        self.bucket_mock.objects.all.return_value = []  # empty bucket

        with self._patch_fs():
            chanterelle.sync_bucket(
                self.bucket_mock, self.mime_types, **self.SETTINGS)

        self.bucket_mock.put_object.assert_called_once_with(
            Key='index.html',
            ContentType=unittest.mock.ANY,
            Body=unittest.mock.ANY)

    def test_e_tag_diff(self):
        """Files are only uploaded if file and bucket ETags don't match."""
        with self._patch_fs(contents=b'Not an empty file.'):
            chanterelle.sync_bucket(
                self.bucket_mock, self.mime_types, **self.SETTINGS)

        self.bucket_mock.put_object.assert_called_once_with(
            Key='index.html',
            ContentType=unittest.mock.ANY,
            Body=unittest.mock.ANY)

    def test_e_tag_no_diff(self):
        """Files are not uploaded if file and bucket ETags match."""
        with self._patch_fs():
            chanterelle.sync_bucket(
                self.bucket_mock, self.mime_types, **self.SETTINGS)

        self.bucket_mock.put_object.assert_not_called()

    def test_delete_old(self):
        """Files not present locally are deleted when delete_old is True."""
        settings = self.SETTINGS.copy()
        settings['delete_old'] = True

        with unittest.mock.patch('os.walk', return_value=[]):
            chanterelle.sync_bucket(
                self.bucket_mock, self.mime_types, **settings)

        self.bucket_mock.delete_objects.assert_called_once_with(
            Delete={'Objects': [{'Key': 'index.html'}]})

    def test_dont_delete_old(self):
        """Files not present locally are ignored when delete_old is False."""
        with unittest.mock.patch('os.walk', return_value=[]):
            chanterelle.sync_bucket(
                self.bucket_mock, self.mime_types, **self.SETTINGS)

        self.bucket_mock.delete_objects.assert_not_called()

    @contextlib.contextmanager
    def _patch_fs(self, path='_site/index.html', contents=b''):
        """Patch os.walk and builtins.open with a single "file"."""
        root, filename = os.path.split(path)
        walk_context = unittest.mock.patch(
            'os.walk', return_value=[(root, [], [filename])])
        open_context = unittest.mock.patch(
            'builtins.open', unittest.mock.mock_open(read_data=contents))

        # enter the patch contexts for the duration of this context
        with walk_context, open_context:
            yield


class WalkPathsTest(unittest.TestCase):
    """Tests for walk_paths()."""

    def test_paths(self):
        """Path generator is created from os.walk() output."""
        walk_generator = [
            ('_site', ['images'], ['index.html', 'about.html']),
            ('_site/images', [], ['logo.png', 'image.jpg', 'banner.png'])
        ]

        paths = list(chanterelle.walk_file_paths(walk_generator))

        self.assertEqual(paths, [
            '_site/index.html',
            '_site/about.html',
            '_site/images/logo.png',
            '_site/images/image.jpg',
            '_site/images/banner.png'
        ])


class MakeKeyTest(unittest.TestCase):
    """Tests for make_key()."""

    def test_strip_html(self):
        """If strip_html is True ".html" suffix will be removed."""
        key = chanterelle.make_key('_site', '_site/index.html', True)

        self.assertEqual(key, 'index')

    def test_dont_strip_html(self):
        """If strip_html is False ".html" suffix will not be removed."""
        key = chanterelle.make_key('_site', '_site/index.html', False)

        self.assertEqual(key, 'index.html')


class GuessContentTypeTest(unittest.TestCase):
    """Tests for guess_content_type()."""

    def setUp(self):
        self.mime_types = mimetypes.MimeTypes()
        self.mime_types.types_map = ({}, {
            '.html': 'text/html',
            '.png': 'image/png'
        })  # replace ALL types

    def test_with_charset(self):
        """Charset will be assigned to a mime subtype of "text"."""
        content_type = chanterelle.guess_content_type(
            'index.html', self.mime_types, 'utf-8')

        self.assertEqual(content_type, 'text/html; charset=utf-8')

    def test_without_charset(self):
        """Charset will not be assigned to non-"text" mime subtypes."""
        content_type = chanterelle.guess_content_type(
            'index.png', self.mime_types, 'utf-8')

        self.assertEqual(content_type, 'image/png')

    def test_unknown_type(self):
        """Raise ValueError if the mime type of a file cannot be determined."""
        with self.assertRaises(ValueError):
            chanterelle.guess_content_type(
                'index.jpg', self.mime_types, 'utf-8')


class CalcETagTest(unittest.TestCase):
    """Tests for calc_e_tag()."""

    def test_e_tag(self):
        """An S3 ETag is a double quoted MD5 hash of file contents."""
        file = io.BytesIO(b'This is the file contents.')

        e_tag = chanterelle.calc_e_tag(file)

        self.assertEqual(e_tag, '"97b2e0d5f8d156cf133b2bcc9098963c"')


class ValidateConfigTest(unittest.TestCase):
    """Tests for validate_config()."""

    def test_valid(self):
        """No ValueError will be raised for a valid config."""
        yaml_config = {'bucket': 'test.bucket'}

        try:
            chanterelle.validate_config(yaml_config)
        except ValueError:
            self.fail()

    def test_incorrect_structure(self):
        """Raise ValueError if YAML file does not contain a root object."""
        yaml_config = 'root is string'

        with self.assertRaises(ValueError):
            chanterelle.validate_config(yaml_config)

    def test_missing_bucket(self):
        """Raise ValueError if YAML file is missing bucket value."""
        yaml_config = {'test_key': 'test_value'}

        with self.assertRaises(ValueError):
            chanterelle.validate_config(yaml_config)


if __name__ == '__main__':
    unittest.main()
